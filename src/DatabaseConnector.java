import java.util.Scanner;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.PreparedStatement;

public class DatabaseConnector { // Class f�r DB connetion och funktiner f�r DB
	private static String database = "travelbooking";
	private static Connection conn;
	private static Statement stmt;
	private static Scanner input = new Scanner(System.in);
	private static String url = "jdbc:mysql://localhost:3306/";
	private static String user = "root";
	private static String pass = "magnus74";
	private static java.sql.PreparedStatement pstmt;
	
	public DatabaseConnector() { // s�tter upp connection till DB

		try {

			conn = DriverManager.getConnection(url, user, pass);
			Class.forName("com.mysql.jdbc.Driver");
			stmt = conn.createStatement();

		} catch (ClassNotFoundException | SQLException exc) {
			exc.printStackTrace();

		}
	}
	
	public void databaseSetup() { // S�tter upp databasen.

		try {

			stmt.executeUpdate("Create database if not exists " + database
					+ ";");
			stmt.executeUpdate("Create table if not exists "
					+ database
					+ ".airport(airport_ID int PRIMARY KEY auto_increment, airport_name VARCHAR(3) UNIQUE NOT NULL, location VARCHAR(15));");

			stmt.executeUpdate("Create table if not exists "
					+ database
					+ ".flight(flight_NR int PRIMARY KEY auto_increment, "
					+ " airline VARCHAR(25), departure_airport VARCHAR(3), departure VARCHAR(15), destination_airport VARCHAR(3), destination VARCHAR(15), departure_date date);");
			stmt.executeUpdate("Create table if not exists travelbooking.bookings(flight_NR int PRIMARY KEY auto_increment, last_name VARCHAR(15), first_name VARCHAR(15), "
					+ "airline VARCHAR(25), departure_airport VARCHAR(3), departure VARCHAR(15), destination_airport VARCHAR(3), destination VARCHAR(15), departure_date date);");

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	public void airportInsert(String airport, String location) { // F�r in data i airport table i DB

		try {

			String sql = "INSERT INTO " + database
					+ ".airport (airport_name,location) ";
			sql += "VALUES('" + airport + "','" + location + "')";
			stmt.executeUpdate(sql);

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	public void airlineInsert(String airline) { // F�r in data i airline table i DB

		try {

			String sql = "INSERT INTO " + database + ".flight (airline) ";
			sql += "VALUES('" + airline + "')";
			stmt.executeUpdate(sql);

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	public void flightInsert(String departure_airport, String departure,
			String destination_airport, String destination, String date,
			int Airline) { // f�r in data i DB f�r flights

		try {

			String sql = "UPDATE " + database
					+ ".flight SET departure_airport = '" + departure_airport
					+ "', departure = '" + departure
					+ "', destination_airport = '" + destination_airport
					+ "', destination = '" + destination
					+ "', departure_date = '" + date + "' WHERE flight_NR='"
					+ Airline + "' ";

			stmt.executeUpdate(sql);

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	public void bookInsert(String departure_airport, String departure,
			String destination_airport, String destination, String date,
			int bookInput) { // Skapar bokningar i DB

		try {

			String sql = "UPDATE " + database
					+ ".bookings SET departure_airport = '" + departure_airport
					+ "', departure = '" + departure
					+ "', destination_airport = '" + destination_airport
					+ "', destination = '" + destination
					+ "', departure_date = '" + date + "' WHERE flight_NR='"
					+ bookInput + "' ";

			stmt.executeUpdate(sql);

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	public void showAirport() { // Metod som printar ut alla Airports
		try {

			String strSelect = "select * from " + database
					+ ".airport ORDER BY airport_ID";

			ResultSet rset = stmt.executeQuery(strSelect);

			while (rset.next()) {
				String airport_ID = rset.getString("airport_ID");
				String airport_name = rset.getString("airport_name");
				String location = rset.getString("location");
				System.out.println(airport_ID + ". " + airport_name + " " + location);

			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}
	
	public void delAirport() { // Metod som tarbort Airports fr�n DB
		try {

			int delIn = input.nextInt();
			String strSelect = ("DELETE from " + database
					+ ".airport WHERE airport_ID LIKE " + delIn);

			stmt.executeUpdate(strSelect);

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}
	
	public void showAirline() { // Metod som printar ut alla Airlines
		try {

			String strSelect = "select * from " + database
					+ ".flight ORDER BY flight_NR";

			ResultSet rset = stmt.executeQuery(strSelect);

			System.out.println("All airlines:");
			while (rset.next()) {
				String flight_NR = rset.getString("flight_NR");
				String airline = rset.getString("airline");
				System.out.println(flight_NR + ". " + airline);

			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}
	
	public void chooseAirline() { // Metod f�r att kunna v�lja olika Airlines via s�kfunkioner.
		try {

			int delIn = input.nextInt();
			String strSelect = ("SELECT from " + database
					+ ".airline WHERE flight_NR LIKE " + delIn);

			stmt.executeQuery(strSelect);

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}
	
	public void delAirline() { // Metod f�r att kunna v�lja och deleta ett Flygbolag
		try {

			int delIn = input.nextInt();
			String strSelect = ("DELETE from " + database
					+ ".airline WHERE airline_ID LIKE " + delIn);

			stmt.executeUpdate(strSelect);

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}
	
	public void showFlight() { // Metod som printar ut alla flights
		try {

			String strSelect = "select * from " + database
					+ ".flight ORDER BY flight_NR";

			ResultSet rset = stmt.executeQuery(strSelect);

			System.out.println("All flights:");
			while (rset.next()) {
				String flight_NR = rset.getString("flight_NR");
				String departure = rset.getString("departure");
				String destination = rset.getString("destination");
				String date = rset.getString("departure_date");
				System.out.println(flight_NR + ". " + departure + "-"
						+ destination + " " + date);

			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	public void delFlight() { // Metod som tarbort en flight som man valt i menyn.
		try {

			int delIn = input.nextInt();
			String strSelect = ("DELETE from " + database
					+ ".flight WHERE flight_NR LIKE " + delIn);

			stmt.executeUpdate(strSelect);

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}
	
	public void flightBooking(int delIn) { // Metod f�r print av flights.
		try {

			String strSelect = ("SELECT * from " + database
					+ ".flight WHERE flight_NR LIKE " + delIn);

			stmt.executeQuery(strSelect);
			ResultSet rset = stmt.executeQuery(strSelect);

			while (rset.next()) {
				String flight_NR = rset.getString("flight_NR");
				String departure = rset.getString("departure");
				String destination = rset.getString("destination");
				System.out.println(flight_NR + ". " + departure + "-"
						+ destination);

			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	public void bookingInsert(String last_name, String first_name, int bookInput) { // Metod som flyttar �ver vald data fr�n flights till booking, f�r att skapa en bookning av en passagerare 
		try {

			String sql = "INSERT INTO " + database
					+ ".bookings (last_name, first_name) ";
			sql += "VALUES('" + last_name + "','" + first_name + "')";

			pstmt = conn.prepareStatement(sql, new String[] { "flight_NR" });
			pstmt.executeUpdate();

			int pk = 0;
			ResultSet genKeys = pstmt.getGeneratedKeys();
			if (null != genKeys && genKeys.next()) {
				pk = genKeys.getInt(1);
			}
			String strSelect = "select * from " + database
					+ ".flight WHERE flight_NR='" + bookInput + "'";

			ResultSet rset = stmt.executeQuery(strSelect);
			String airline = "";
			String departure_airport = "";
			String departure = "";
			String destination_airport = "";
			String destination = "";
			String date = "";

			while (rset.next()) {
				airline = rset.getString("airline");
				departure_airport = rset.getString("departure_airport");
				departure = rset.getString("departure");
				destination_airport = rset.getString("destination_airport");
				destination = rset.getString("destination");
				date = rset.getString("departure_date");

			}

			sql = "UPDATE " + database + " .bookings " + "SET " + database
					+ ".bookings.airline = '" + airline + "'," + database
					+ " .bookings.departure_airport = '" + departure_airport
					+ "'," + database + " .bookings.departure = '" + departure
					+ "'," + database + " .bookings.destination_airport = '"
					+ destination_airport + "'," + database
					+ " .bookings.destination = '" + destination + "',"
					+ database + " .bookings.departure_date = '" + date + "'"
					+ "WHERE flight_NR=" + pk;

			stmt.executeUpdate(sql);

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}
	
	public void showBookings() { // Metod f�r att printa ut dina bokningar.

		try {

			String strSelect = "select * from " + database
					+ ".bookings ORDER BY flight_NR";

			ResultSet rset = stmt.executeQuery(strSelect);

			System.out.println("Your Bookings:");
			System.out
					.println("-------------------------------------------------------------------------------------------");
			while (rset.next()) {
				String flight_NR = rset.getString("flight_NR");
				String last_name = rset.getString("last_name");
				String first_name = rset.getString("first_name");
				String airline = rset.getString("airline");
				String departure_airport = rset.getString("departure_airport");
				String departure = rset.getString("departure");
				String destination_airport = rset
						.getString("destination_airport");
				String destination = rset.getString("destination");
				String departure_date = rset.getString("departure_date");
				System.out.println(flight_NR + "\t" + last_name + "\t"
						+ first_name + "\t" + airline + "\t"
						+ departure_airport + " " + departure + "-"
						+ destination_airport + " " + destination + "\t"
						+ departure_date);

			}
			System.out
					.println("-------------------------------------------------------------------------------------------");

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	public void delBooking() { // Metod f�r att kunna ta bort bokningar. 

		try {

			int delIn = input.nextInt();
			String strSelect = ("DELETE from " + database
					+ ".bookings WHERE flight_NR LIKE " + delIn);

			stmt.executeUpdate(strSelect);

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	public void departureSearch() { // Metod f�r att kunna s�ka efter avg�ngar.

		try {

			String search = input.next();
			String strSelect = ("SELECT * from " + database
					+ ".flight WHERE departure LIKE '" + search + "'");

			stmt.executeQuery(strSelect);
			ResultSet rset = stmt.executeQuery(strSelect);

			while (rset.next()) {
				String flight_NR = rset.getString("flight_NR");
				String departure = rset.getString("departure");
				String destination = rset.getString("destination");
				String departure_date = rset.getString("departure_date");
				System.out.println(flight_NR + ". " + departure + "-"
						+ destination + " " + departure_date);

			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}
	
	public void destinationSearch() { // Metod f�r att kunna s�ka efter resor via destination.

		try {

			String search = input.next();
			String strSelect = ("SELECT * from " + database
					+ ".flight WHERE destination LIKE '" + search + "'");

			stmt.executeQuery(strSelect);
			ResultSet rset = stmt.executeQuery(strSelect);

			while (rset.next()) {
				String flight_NR = rset.getString("flight_NR");
				String departure = rset.getString("departure");
				String destination = rset.getString("destination");
				String departure_date = rset.getString("departure_date");
				System.out.println(flight_NR + ". " + departure + "-"
						+ destination + " " + departure_date);

			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	public void ariportSelect() {

	}
}
	

	/*public void departureIn(int departureIn){
		
		try{
		
			
		pstmt = conn.prepareStatement(sql, new String[] { "flight_NR" });
		pstmt.executeUpdate();

		int pk = 0;
		ResultSet genKeys = pstmt.getGeneratedKeys();
		if (null != genKeys && genKeys.next()) {
			pk = genKeys.getInt(1);
		}
		String strSelect = "select * from " + database
				+ ".airport WHERE airport_ID='" + departureIn + "'";

		ResultSet rset = stmt.executeQuery(strSelect);
		//String airline = "";
		String departure_airport = "";
		String departure = "";
		//String destination_airport = "";
		//String destination = "";
		//String date = "";

		while (rset.next()) {
			//airline = rset.getString("airline");
			departure_airport = rset.getString("airport_name");
			departure = rset.getString("location");
			//destination_airport = rset.getString("destination_airport");
			//destination = rset.getString("destination");
			//date = rset.getString("departure_date");

		}

		String sql = "UPDATE " + database + " .flight " 
		+ "SET " + database + " .bookings.departure_airport = '" + departure_airport 
		+ "'," + database + " .bookings.departure = '" + departure + "',"
		+ "WHERE flight_NR=" + pk;
		pstmt = conn.prepareStatement(sql);
		pstmt.executeUpdate(sql);

	} catch (SQLException ex) {
		ex.printStackTrace();
	}

}
	
}*/