
import java.util.regex.Pattern;
// Regex class
public class Regex {
	// regex pattern f�r Airport insert
	public static boolean regexAirport(String s) {

		Pattern pattern = Pattern.compile("[a-�A-�]{3}");

		return pattern.matcher(s).matches();

	}
	
	public static boolean regexAirline(String s) { //	Metod med regex pattern f�r airline insert

		Pattern pattern = Pattern.compile("[a-�A-�0-9+\\s]{6,25}");

		return pattern.matcher(s).matches();

	}
	
	public static boolean regexDate(String s){ // Metod med regex pattern f�r Datum insert
		
		Pattern pattern = Pattern.compile("[0-9]{4}-[0-9]{2}-[0-9]{2}"); //"([0-9]{4}\\./-\\s\\+[0-9]{2}\\./-\\s\\+[0-9]{2})";

		return pattern.matcher(s).matches();
	}
	
	public static boolean regexDestination(String s) { // Metod med regex pattern f�r destinations insert.

		Pattern pattern = Pattern.compile("[a-�A-�0-9+\\s]{1,30}");

		return pattern.matcher(s).matches();
	}
}



